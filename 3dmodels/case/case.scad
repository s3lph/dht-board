
use </home/sebastian/Programming/esp8266/wifitest/3dmodels/dht22.scad>

$fn = 50;

module screwhole(ri=1, ro=1.7, h=1.8) {
  difference() {
    cylinder(r=ro, h=h);
    translate([0,0,-0.1]) {
      cylinder(r=ri, h=h+0.2);
    }
  }
}

module screwhole_rect(ri=1, ro=2, h=3, ox=5, oy=5) {
  difference() {
    translate([-ro,-ro,0]) {
      cube([ro+ox,ro+oy,h]);
    }
    translate([0,0,-0.1]) {
      cylinder(r=ri, h=h+0.2);
    }
  }
}

module screwholes_rect(h=3.2) {
  union() {  
    translate([-4.45,4.45,-3.3]) {
      rotate([0,0,-90]) {
        screwhole_rect(h=h);
      }
    }
    translate([-4.45,34.92,-3.3]) {
      rotate([0,0,0]) {
        screwhole_rect(h=h);
      }
    }
    translate([-55.25,4.45,-3.3]) {
      rotate([0,0,180]) {
        screwhole_rect(h=h);
      }
    }
    translate([-55.25,34.92,-3.3]) {
      rotate([0,0,90]) {
        screwhole_rect(h=h);
      }
    }
  }
}

module screwholes(h=1.9) {
  union() {
    translate([0,0,-.1]) {
      translate([-4.45,4.45,-0.1]) {
        screwhole(h=h);
      }
      translate([-4.45,34.92,-0.1]) {
        screwhole(h=h);
      }
      translate([-55.25,4.45,-0.1]) {
        screwhole(h=h);
      }
      translate([-55.25,34.92,-0.1]) {
        screwhole(h=h);
      }
    }
  }
}

//
//  BOTTOM COVER
//

module bottom() {
//  oh = -13.8;
  oh = -16;
  translate([-61.8,-2.1,oh]) {
    difference() {
      union() {
        cube([63.9,43.6,0.5]);
        translate([2,2,0]) {
          cube([59.9,39.6,1.5]);
        }
      }
      
      translate([1,4,0.5]) {
        cube([65.9,35.6,2]);
      }
      translate([4,1,0.5]) {
        cube([55.9,45.6,2]);
      }

    }
  }
}

//
//  MIDDLE PART
//

module middle() {

  union() {
    screwholes();
    screwholes_rect();
    
    // outer walls
    translate([-61.8,-2.1,-13.3]) {
      cube([63.9,2,15]);
    }
    translate([-61.8,39.5,-13.3]) {
      cube([63.9,2,15]);
    }
    translate([.1,-2.1,-13.3]) {
      cube([2,43.6,15]);
    }
    translate([-61.8,-2.1,-13.3]) {
      cube([2,43.6,15]);
    }
    
    // thin floor
    translate([-60,6,-3.3]) {
      cube([62,27,0.5]);
    }
    translate([-54,30,-3.3]) {
      cube([48,10,0.5]);
    }
    translate([-42,-0.1,-3.3]) {
      cube([3,8,0.5]);
    }
    
    // inner walls
    translate([-39.5,6,-12.3]) {
      cube([41,0.5,9.5]);
    }
    translate([-39.5,-1,-12.3]) {
      cube([0.5,7,9.5]);
    }
    translate([0,0,-13.1]) {
      screwholes(h=10.1);
    }
    
  }

}

//
//  TOP COVER
//

module top() {

  union() {
    oh = 2;
    h = 7;
    // outer walls
    translate([0,0,oh]) {
      translate([-61.8,-2.1,0]) {
        cube([63.9,2,h]);
      }
      translate([-61.8,39.5,0]) {
        cube([63.9,2,h]);
      }
      translate([.1,-2.1,0]) {
        cube([2,43.6,h]);
      }
      translate([-61.8,-2.1,0]) {
        cube([2,43.6,h]);
      }
      translate([0,0,3.3]) {
        screwholes_rect(h=h);
      }
      // roof
      difference() {
        union() {
          translate([-60,6,h-0.5]) {
            cube([62,27,0.5]);
          }
          translate([-54,-1,h-0.5]) {
            cube([48,42,0.5]);
          }
        }
        // DHT hole
        translate([-55.5,15,h-0.6]) {
          cube([22,17.1,1]);
        }
        // Reset hole
        translate([-14.5,34.8,h-0.6]) {
          cylinder(r=0.75,h=1);
        }
      }
    }
  }

}


#import("/tmp/untitled1.stl");
#translate([-34.4,31,2]) {
  rotate([0,0,180]) {
    dht22(fold=6.5);
  }
}


top();
middle();
bottom();
