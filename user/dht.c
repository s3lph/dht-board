
#include <os_type.h>
#include <osapi.h>
#include <gpio.h>

#include "dht.h"

#define DHT_PIN PERIPHS_IO_MUX_GPIO2_U
#define DHT_BIT_THRESH 60

os_timer_t nextfn_timer, timeout_timer;
dht_result_cb_t result_callback;
dht_fail_cb_t fail_callback;

uint32 bit_counts[80];
static uint8 bit_count;

static void ICACHE_FLASH_ATTR timeout(void *arg) {
  os_timer_disarm(&nextfn_timer);
  os_timer_disarm(&timeout_timer);
  fail_callback(DHT_STATUS_TIMEOUT);
}

static void ICACHE_FLASH_ATTR finalize_measurement(void *arg) {
  os_timer_disarm(&nextfn_timer);
  os_timer_disarm(&timeout_timer);
  uint8 byte1, byte2, byte3, byte4, byte5, checksum, i;
  int32 humidity, temperature;
  for (i = 0; i < 8; ++i) {
    byte1 = (byte1 << 1) | (bit_counts[2*i+ 1] - bit_counts[2*i   ]) > DHT_BIT_THRESH;
    byte2 = (byte2 << 1) | (bit_counts[2*i+17] - bit_counts[2*i+16]) > DHT_BIT_THRESH;
    byte3 = (byte3 << 1) | (bit_counts[2*i+33] - bit_counts[2*i+32]) > DHT_BIT_THRESH;
    byte4 = (byte4 << 1) | (bit_counts[2*i+49] - bit_counts[2*i+48]) > DHT_BIT_THRESH;
    byte5 = (byte5 << 1) | (bit_counts[2*i+65] - bit_counts[2*i+64]) > DHT_BIT_THRESH;
  }
  checksum = (uint8) ((byte1 + byte2 + byte3 + byte4) & 0xff);
  if (checksum == byte5) {
#if defined(HT_11)
    if (byte2 > 9 || byte4 > 9) {
      fail_callback(DHT_STATUS_RANGE);
      return;
    }
    humidity = byte1 * 10 + byte2;
    temperature = byte3 * 10 + byte4;
#elif defined(HT_22)
    humidity = (byte1 << 8) | byte2;
    temperature = ((byte3 & 0x7f) << 8) | byte4;
    if (byte3 & 0x80) {
      temperature = -temperature;
    }
#else
#error "Must defined either -DHT_11 or -DHT_22"
#endif
    if (humidity < 0 || humidity > 1000) {
      fail_callback(DHT_STATUS_RANGE);
      return;
    }
    result_callback(temperature, humidity);
  } else {
    os_printf("DHT checksum mismatch: computed: %d, received: %d\n", checksum, byte5);
    fail_callback(DHT_STATUS_CHECKSUM);
  }
}

static void ICACHE_FLASH_ATTR dht_interrupt(void *arg) {
  uint8 state = GPIO_INPUT_GET(2);
  if (bit_count % 2 == state) {
    uint32 time = system_get_time();
    if (bit_count > 2) {
      bit_counts[bit_count-3] = time;
    }
    ++bit_count;
    if (bit_count >= 83) {
      ETS_GPIO_INTR_DISABLE();
      os_timer_setfn(&nextfn_timer, finalize_measurement, NULL);
      os_timer_arm(&nextfn_timer, 100, 0);
    }
  }

  uint32 gpio_status = GPIO_REG_READ(GPIO_STATUS_ADDRESS);
  GPIO_REG_WRITE(GPIO_STATUS_W1TC_ADDRESS, gpio_status);
}


static void end_dht_init(void *arg) {
  bit_count = 0;
  os_timer_disarm(&nextfn_timer);
  //gpio_output_set(0, BIT2, 0, BIT2);
  gpio_output_set(BIT2, 0, 0, BIT2);
  gpio_pin_intr_state_set(GPIO_ID_PIN(2), GPIO_PIN_INTR_ANYEDGE);
  ETS_GPIO_INTR_ATTACH(dht_interrupt, NULL);
  ETS_GPIO_INTR_ENABLE();
}

static void begin_dht_init(void *arg) {
  os_timer_disarm(&nextfn_timer);
  os_timer_disarm(&timeout_timer);
  os_timer_setfn(&timeout_timer, timeout, NULL);
  os_timer_arm(&timeout_timer, 1000, 0);
  gpio_output_set(0, BIT2, BIT2, 0);
  os_timer_setfn(&nextfn_timer, end_dht_init, NULL);
  os_timer_arm(&nextfn_timer, 20, 0);
  os_printf("dht low\n");
}

void dht_read(dht_result_cb_t result, dht_fail_cb_t fail) {
  result_callback = result;
  fail_callback = fail;
  ETS_GPIO_INTR_DISABLE();
  PIN_FUNC_SELECT(PERIPHS_IO_MUX_GPIO2_U, FUNC_GPIO2);
  PIN_PULLUP_EN(PERIPHS_IO_MUX_GPIO2_U);
  gpio_output_set(BIT2, 0, BIT2, 0);
  os_timer_disarm(&timeout_timer);
  os_timer_disarm(&nextfn_timer);
  os_timer_setfn(&nextfn_timer, begin_dht_init, NULL);
  os_timer_arm(&nextfn_timer, 3000, 0);
  os_printf("dht high timer start\n");
}
