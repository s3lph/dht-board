#include <os_type.h>
#include <osapi.h>
#include <user_interface.h>
#include <espconn.h>
#include <ip_addr.h>

#include "dht.h"

struct scan_config scan_conf = {
                                .ssid = "kabelsalat-2ghz",
                                .bssid = NULL,
                                .channel = 0,
                                .show_hidden = 1
};

struct station_config sta_conf = {
                                  .ssid = "kabelsalat-2ghz",
                                  .password = "club mate cola",
                                  .bssid_set = 0,
                                  .bssid = ""
};

void reset_cb(void *arg) {
  system_restart();
}

static os_timer_t reset;

static esp_tcp tcp;
static struct espconn con;
static struct ip_addr myip, remoteip;
static char body[200];
static char data[800];

static os_event_t queue;

#ifdef _SENSOR_HACKCENTER
static const char *sensor = "Hackcenter";
#elif defined _SENSOR_RACK
static const char *sensor = "Server%20Rack";
#elif defined _SENSOR_WORKSHOP
static const char *sensor = "Workshop";
#elif defined _SENSOR_OUTSIDE
static const char *sensor = "Outside";
#endif

void done() {
  // Turn off DHT
  gpio_output_set(0, BIT2|BIT4, 0, BIT2|BIT4);
  // Disable WiFi
  wifi_set_opmode(NULL_MODE);
  // 1 minute
  system_deep_sleep(60000000);
  //os_timer_disarm(&reset);
  //os_timer_setfn(&reset, reset_cb, NULL);
  //os_timer_arm(&reset, 60000, 0);
}

void connect(void *arg) {
  os_printf("tcp connected, sending data\n");
  if (espconn_send(&con, data, os_strlen(data))) {
    os_printf("tcp send failed\n");
  }
}

void user_task(os_event_t *evt) {
  switch(evt->sig) {
  case 0:
    os_printf("tcp disconnect\n");
    espconn_disconnect(&con);
    break;
  default:
    break;
  }
}

void sent(void *arg) {
  os_printf("tcp sent\n");
  system_os_post(USER_TASK_PRIO_0, 0, '0');
}

void disconnect(void *arg) {
  os_printf("tcp disconnected\n");
  espconn_delete(&con);
  wifi_station_disconnect();
}

void start_tcp() {
  int i;
  tcp.remote_port = 8086;
  tcp.local_port = espconn_port();
  tcp.remote_ip[0] = 10;
  tcp.remote_ip[1] = 20;
  tcp.remote_ip[2] = 0;
  tcp.remote_ip[3] = 239;
  con.type = ESPCONN_TCP;
  con.state = ESPCONN_NONE;
  con.proto.tcp = &tcp;
  for (i = 0; i < 4; ++i) {
    tcp.local_ip[i] = (uint8) ((myip.addr >> (i*8)) & 0xff);
  }
  
  espconn_regist_sentcb(&con, sent);
  espconn_regist_connectcb(&con, connect);
  espconn_regist_disconcb(&con, disconnect);
  os_printf("connecting to server\n");
  if (espconn_connect(&con)) {
    os_printf("espconn_connect failed\n");
  }
}

void dht_fail(enum dht_status reason) {
  done();
}

void dht_done(int32 t, int32 h) {
  size_t bodylen;
  os_printf("dht read done\n");
  os_sprintf(body,
             "temperature,location=%s value=%d.%d\nhumidity,location=%s value=%d.%d\n",
             sensor, t / 10, t % 10, sensor, h / 10, h % 10);
  bodylen = strlen(body);
  os_printf("http body length: %u\n", bodylen);
  os_sprintf(data,
             "POST /write?db=sensors&precision=s&u=sensors&p=SbNdVGvgXws3xGrd HTTP/1.1\r\nHost: status.lan.kabelsalat.ch:8086\r\nContent-Type: application/x-www-form-urlencoded\r\nContent-Length: %u\r\nConnection: close\r\n\r\n%s",
             bodylen, body);
  bodylen = strlen(data);
  os_printf("http buffer length: %u\n", bodylen);
  start_tcp();
}

void wifi_event_handler(System_Event_t *event) {
  if (event->event == EVENT_STAMODE_CONNECTED) {
    os_printf("wifi connected\n");
    wifi_station_dhcpc_start();
  } else if (event->event == EVENT_STAMODE_DISCONNECTED) {
    os_printf("wifi disconnected\n");
    done();
  } else if (event->event == EVENT_STAMODE_GOT_IP) {
    os_printf("wifi dhcpc success\n");
    myip = event->event_info.got_ip.ip;
    dht_read(dht_done, dht_fail);
  } else if (event->event == EVENT_STAMODE_DHCP_TIMEOUT) {
    wifi_station_disconnect();
    done();
  }
}

void wifi_scan_done(void *arg, STATUS status) {
  os_printf("wifi scan done\n");
  wifi_station_set_config_current(&sta_conf);
  wifi_set_event_handler_cb(wifi_event_handler);
  wifi_station_connect();
}

void sdk_init_done_cb(void) {
  os_printf("user_init done\n");
  system_os_task(user_task, USER_TASK_PRIO_0, &queue, 1);
  gpio_init();
  // Turn on DHT
  gpio_output_set(BIT2|BIT4, 0, BIT2|BIT4, 0);
  wifi_set_opmode_current(STATION_MODE);
  wifi_station_scan(&scan_conf, wifi_scan_done);
  os_printf("dht read initiated\n");
}

void user_init() {
  uart_div_modify(0, UART_CLK_FREQ / 115200);
  wifi_set_opmode(NULL_MODE);
  system_init_done_cb(sdk_init_done_cb);
}
