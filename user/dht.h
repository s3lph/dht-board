
#ifndef __DHT_H__
#define __DHT_H__

enum dht_status {
  DHT_STATUS_OK,
  DHT_STATUS_CHECKSUM,
  DHT_STATUS_TIMEOUT,
  DHT_STATUS_RANGE
};

typedef void (*dht_result_cb_t) (int32 temperature, int32 humidity);
typedef void (*dht_fail_cb_t) (enum dht_status reason);

void dht_read(dht_result_cb_t callback, dht_fail_cb_t fail);

#endif // __DHT_H__
