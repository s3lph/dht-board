#!/usr/bin/env python3

import sys
import os

import pcbnew


def plot_gerber(project_name: str, pcb_file: str, output_dir: str) -> None:
    projectname: str = project_name.upper()
    basename: str = os.path.basename(pcb_file).replace('.kicad_pcb', '')
    layers = [
        ('GTL', pcbnew.F_Cu, 'Top layer'),
        ('GBL', pcbnew.B_Cu, 'Bottom layer'),
        ('GTO', pcbnew.F_SilkS, 'Silk top'),
        ('GBO', pcbnew.B_SilkS, 'Silk top'),
        ('GTS', pcbnew.F_Mask, 'Mask top'),
        ('GBS', pcbnew.B_Mask, 'Mask bottom'),
        ('GBR', pcbnew.Edge_Cuts, 'Edges')
    ]
    os.makedirs(output_dir, exist_ok=True)
    
    board = pcbnew.LoadBoard(pcb_file)
    pctl = pcbnew.PLOT_CONTROLLER(board)
    popt = pctl.GetPlotOptions()

    popt.SetOutputDirectory(output_dir)

    for layer_info in layers:
        pctl.SetLayer(layer_info[1])
        pctl.OpenPlotfile(f'{layer_info[0]}', pcbnew.PLOT_FORMAT_GERBER, layer_info[2])
        pctl.PlotLayer()
    pctl.ClosePlot()

    gwr = pcbnew.EXCELLON_WRITER(board)
    gwr.SetFormat(
        True  # metric
    )
    gwr.SetOptions(
        False,  # mirror
        False,  # minimal header
        pcbnew.wxPoint(0, 0),  # offset
        True  # merge pth/npth
    )
    gwr.CreateDrillandMapFilesSet(output_dir, True, False)

    for layer_info in layers:
        src = os.path.join(output_dir, f'{basename}-{layer_info[0]}.gbr')
        dest = os.path.join(output_dir, f'{projectname}.{layer_info[0]}')
        os.rename(src, dest)
    src = os.path.join(output_dir, f'{basename}.drl')
    dest = os.path.join(output_dir, f'{projectname}.TXT')
    os.rename(src, dest)
    

def main() -> None:
    if len(sys.argv) < 4:
        print(f'Usage: {sys.argv[0]} <projectname> <project.kicad_pcb> <output/>')
        exit(1)
    project_name: str = sys.argv[1]
    pcb_file: str = os.path.abspath(sys.argv[2])
    output_dir: str = os.path.abspath(sys.argv[3])
    plot_gerber(project_name, pcb_file, output_dir)


if __name__ == '__main__':
    main()
